from optparse import OptionParser
import sys
import chef
import subprocess
from random import choice


def parse_options(args):
    parser = OptionParser()
    parser.add_option("-r", "--role", dest="role",
        help="The chef role to perform cassandra maintenance on.",
        metavar="ROLE")
    parser.add_option("-p", "--repair", dest="repair", default=False,
        action="store_true", help="Force repair on the node.")
    parser.add_option("-c", "--compact", dest="compact", default=False,
        action="store_true", help="Force major compaction on the node.")
    parser.add_option("-l", "--cleanup", dest="cleanup", default=False,
        action="store_true", help="Run cleanup on the node.")
    parser.add_option("-f", "--file", dest="file", default="/tmp/cassandra_maint",
        help="The file to store the current node being processed.",
        metavar="FILE")
    parser.add_option("-k", "--keyspace", dest="keyspace",
        help="The keyspace to perform maintenance on. Defaults to all of them.",
        metavar="KEYSPACE")
    parser.add_option("-o", "--column-family", dest="columnfamily",
        help="The column family to perform maintenance on. Defaults to all of them.",
        metavar="COLUMN-FAMILY")
    parser.add_option("-v", "--verbose", dest="verbose",
        help="Use verbose logging.", action="store_true",
        default=False)
    parser.add_option("-d", "--dry-run", dest="dryrun", default=False,
        help="Don't actually perform maintenance.", action="store_true")
    parser.add_option("-u", "--chef-server", dest="chefserver",
        default="https://chef.internal.formspring.me",
        help="The chef server to connect to.")
    parser.add_option("-z", "--chef-key", dest="chefkey",
        default="/etc/chef/client.pem",
        help="The chef client file")
    parser.add_option("-x", "--chef-client", dest="chefclient",
        default="admin", help="The chef client name to use.")
    parser.add_option("-a", "--all", dest="all_hosts",
        default=False, action="store_true",
        help="Perform the action on *all* hosts sequentially.")
    return parser.parse_args(args)


def perform_maintenance(host, repair=True, cleanup=True,
                        compact=False, keyspace=None, column_family=None,
                        dryrun=False, verbose=False):
    def show(s):
        if verbose:
            print s

    def perform_command(cmd, keyspace=None, column_family=None):
        mycmd = ["nodetool", "-h", "localhost", cmd]
        if keyspace is not None:
            mycmd.append(keyspace)
        if column_family is not None:
            mycmd.append(column_family)
        cmds = 'ssh %s "%s"' % (host, " ".join(mycmd))
        show("Performing '%s'" % cmds)
        if not dryrun:
            proc = subprocess.Popen(cmds, stdout=subprocess.PIPE, shell=True)
            show(proc.stdout.readlines())

    show("Performing maintenance on %s" % host)
    if repair:
        perform_command("repair", keyspace=keyspace, column_family=column_family)
    if compact:
        perform_command("disablegossip")
        perform_command("disablethrift")
        perform_command("compact", keyspace=keyspace, column_family=column_family)
        perform_command("enablegossip")
        perform_command("enablethrift")
    if cleanup:
        perform_command("cleanup", keyspace=keyspace, column_family=column_family)


def main(argv=sys.argv):
    (options, poop) = parse_options(argv)

    if options.role == None:
        print "What role do you want to like do stuff to?"
        return 2

    if options.columnfamily and not options.keyspace:
        print "You must provide a keyspace if you provide a column family."
        return 3

    current_host = None
    try:
        state_file = open(options.file, 'r')
        current_host = state_file.readline().strip()
        if options.verbose:
            print "The current host in file is %s" % current_host
        state_file.close()
    except IOError:
        pass

    chef_api = chef.ChefAPI(options.chefserver, options.chefkey, options.chefclient)
    results = chef.Search("node", "role:%s" % (str(options.role)), api=chef_api)
    hosts = [x.object['fqdn'] for x in results]
    hosts.sort()
    if options.all_hosts:
        perform_on = hosts
    else:
        if current_host == None or current_host not in hosts:
            if options.verbose:
                print "The host is not in the list or none set. Using random one"
            actual_host = choice(hosts)
        else:
            idx = hosts.index(current_host)
            if idx >= (len(hosts) - 1):
                actual_host = hosts[0]
            else:
                actual_host = hosts[idx + 1]
        f = open(options.file, "w")
        f.write("%s\n" % (actual_host))
        f.close()
        perform_on = [actual_host]
    for h in perform_on:
        perform_maintenance(h, verbose=options.verbose, dryrun=options.dryrun,
                    repair=options.repair, compact=options.compact, cleanup=options.cleanup,
                    column_family=options.columnfamily, keyspace=options.keyspace)

if __name__ == "__main__":
    sys.exit(main())
